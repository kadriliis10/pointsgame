using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using YLEHEKI_API.Models;

namespace YLEHEKI_API.Data
{
    public class Initializer
    {
        public static void Init(DataContext? context)
        {
            if(context == null)
            {
                return;
            }

            var popularGames = new List<PopularGame>
            {
                new PopularGame()
                {
                    Id = 1,
                    User= "",
                    Name = "Meeste 10-võistlus",
                    isPublic = true,
                    Rules = @"1. Kui vähegi võimalik, peab mitmevõistluse peakohtunik tagama
                    igale osavõtjale vähemalt 30-minutilise pausi eelmise ala lõpu ja järgmise ala alguse vahel
                    2. Võistlusjärjekord võidakse loosida enne iga ala algust. 100+
                    ja 400 meetri jooksudes ning 110 meetri tõkkejooksus paigutab
                    võistlejad jooksudesse tehniline delegaat. Seejuures peab igas
                    jooksus olema 5 või enam, kuid mitte mingil juhul vähem kui 3 võistlejat.
                    3. Mitmevõistluse viimasel alal paigutatakse pärast eelviimast
                    ala liidergruppi kuuluvad võistlejad ühte jooksu. Teised
                    võistlejad võidakse paigutada (loosida) jooksudesse vastavalt
                    sellele, millal nad lõpetavad eelviimase ala. \n
                    4. Mitmevõistluse peakohtunikul on õigus vajaduse korral muuta
                    mis tahes jooksu koosseisu. \n
                    5. Pärast iga ala lõppu tuleb punktiseis (st antud ala
                    tulemusele vastavad punktid IAAF-i ametliku punktitabeli alusel
                    ja punktide summa pärast kõiki seni lõppenud alasid) kõigile
                    võistlejatele teatavaks teha.",
                    Events = new List<Event>
                    {
                        new Event()
                        {
                            Name = "100m jooks",
                            Formula = "25.4347*(18-x)**1.81"
                        },
                        new Event()
                        {
                            Name = "Kaugushüpe",
                            Formula = "0.14354*(x-220)**1.4"
                        },
                        new Event()
                        {
                            Name = "Kuulitõuge",
                            Formula = "51.39*(x-1.5)**1.05"
                        },
                        new Event()
                        {
                            Name = "Kõrgushüpe",
                            Formula = "0.8465*(x-75)**1.42"
                        },
                        new Event()
                        {
                            Name = "400m jooks",
                            Formula = "1.53775*(82-x)**1.81"
                        },
                        new Event()
                        {
                            Name = "110m tõkkejooks",
                            Formula = "5.74352*(28.5-x)**1.92"
                        },
                        new Event()
                        {
                            Name = "Vasaraheide",
                            Formula = "12.91*(x-4)**1.1"
                        },
                        new Event()
                        {
                            Name = "Teivashüpe",
                            Formula = "0.2797*(x-100)**1.35"
                        },
                        new Event()
                        {
                            Name = "Odavise",
                            Formula = "10.14*(x-7)**1.08"
                        },
                        new Event()
                        {
                            Name = "1500m jooks",
                            Formula = "0.03768*(480-x)**1.85"
                        },
                    },
                    PicturePath = "src/images/KevinMayer.jpg"
                },
                new PopularGame()
                {
                    Id = 2,
                    User= "",
                    Name = "Meeste 7-võistlus",
                    isPublic = true,
                    Rules =   @"Meeste seitsmevõistlusesse kuuluvad järgmised alad, mis
                    peetakse kahel järjestikusel päeval järgnevas järjestuses:
                    Esimene päev: 60m jooks, kaugushüpe, kuulitõuge ja kõrgushüp
                    Teine päev: 60m tõkkejooks, teivashüpe ja 1000m jooks.",

                    Events = new List<Event>
                    {
                        new Event()
                        {
                            Name = "60m jooks",
                            Formula = "58.015*(11.5-x)**1.81"
                        },
                        new Event()
                        {
                            Name = "Kaugushüpe",
                            Formula = "0.14354*(x-220)**1.4"
                        },
                        new Event()
                        {
                            Name = "Kuulitõuge",
                            Formula = "51.39*(x-1.5)**1.05"
                        },
                        new Event()
                        {
                            Name = "Kõrgushüpe",
                            Formula = "0.8465*(x-75)**1.42"
                        },
                        new Event()
                        {
                            Name = "60m tõkkejooks",
                            Formula = "20.5173*(15.5-x)**1.92"
                        },
                        new Event()
                        {
                            Name = "Teivashüpe",
                            Formula = "0.2797*(x-100)**1.35"
                        },
                        new Event()
                        {
                            Name = "1000m jooks",
                            Formula = "0.08713*(305.5-x)**1.85"
                        },
                    },
                    PicturePath = "src/images/KristjanHauss.jpg"
                },
                new PopularGame()
                {
                    Id = 3,
                    User= "",
                    Name = "Naiste 7-võistlus",
                    isPublic = true,
                    Rules =   @"Naiste seitsmevõistlus koosneb seitsmest alast, mis peetakse
                    kahel, üksteisele järgneval päeval järgmises järjekorras:
                    Esimene päev - 100m tõkkejooks, kõrgushüpe, kuulitõuge ja 200m
                    jooks; Teine päev - kaugushüpe, odavise ja 800m jooks.",

                    Events = new List<Event>
                    {
                        new Event()
                        {
                            Name = "100m tõkkejooks",
                            Formula = "9.23076*(26.7-x)**1.835"
                        },
                        new Event()
                        {
                            Name = "Kõrgushüpe",
                            Formula = "1.84523*(x-75)**1.348"
                        },
                        new Event()
                        {
                            Name = "Kuulitõuge",
                            Formula = "56.0211*(x-1.5)**1.05"
                        },
                        new Event()
                        {
                            Name = "200m jooks",
                            Formula = "4.99087*(42.5-x)**1.81"
                        },
                        new Event()
                        {
                            Name = "Kaugushüpe",
                            Formula = "0.188807*(x-210)**1.41"
                        },
                        new Event()
                        {
                            Name = "Odavise",
                            Formula = "15.9803*(x-3.8)**1.04"
                        },
                        new Event()
                        {
                            Name = "800m jooks",
                            Formula = "0.11193*(254-x)**1.88"
                        },
                    },
                    PicturePath = "src/images/JohnsonThompson.jpg"
                },
                new PopularGame()
                {
                    Id = 4,
                    User= "",
                    Name = "Naiste 5-võistlus",
                    isPublic = true,
                    Rules =   @"Naiste viievõistlus koosneb viiest alast, mis peetakse
                    ühel päeval järgmises järjekorras: 60m tõkkejooks, kõrgushüpe, kuulitõuge, kaugushüpe ja 800m jooks.",

                    Events = new List<Event>
                    {
                        new Event()
                        {
                            Name = "60m tõkkejooks",
                            Formula = "20.0479*(17-x)**1.835"
                        },
                        new Event()
                        {
                            Name = "Kõrgushüpe",
                            Formula = "1.84523*(x-75)**1.348"
                        },
                        new Event()
                        {
                            Name = "Kuulitõuge",
                            Formula = "56.0211*(x-1.5)**1.05"
                        },
                        new Event()
                        {
                            Name = "Kaugushüpe",
                            Formula = "0.188807*(x-210)**1.41"
                        },
                        new Event()
                        {
                            Name = "800m jooks",
                            Formula = "0.11193*(254-x)**1.88"
                        },
                    },
                    PicturePath = "src/images/PippiLottaEnok.jpg"
                },

            };
            context.PopularGamesList?.AddRange(popularGames);
            context.SaveChanges();
            
        }
    }
}