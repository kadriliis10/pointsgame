﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace YLEHEKI_API.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string? User { get; set; }
        public string? Name { get; set; }
        public bool isPublic { get; set; }
        public string? Rules { get; set; }
        public List<Player>? Players { get; set; }
        public List<Event>? Events { get; set; }
        public string? PicturePath { get; set; }
    }

    public class Player
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public List<Results>? Results { get; set; }
        public List<Points>? Points { get; set; }
    }

    public class Results
    {
        public int Id { get; set; }
        public double Result { get; set; }
    }

    public class Points
    {
        public int Id { get; set; }
        public double Point { get; set; }
    }
    

    public class Event
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        public string? Formula { get; set; }

    }
}