using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YLEHEKI_API.Models
{
    public class PopularGame
    {
        public int Id { get; set; }
        public string? User { get; set; }
        public string? Name { get; set; }
        public bool? isPublic { get; set; }
        public string? Rules { get; set; }
        public List<Player>? Players { get; set; }
        public List<Event>? Events { get; set; }
        public string? PicturePath { get; set; }
    }

}