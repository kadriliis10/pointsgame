using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using YLEHEKI_API.Models;
using static YLEHEKI_API.Models.Game;


namespace YLEHEKI_API.Controllers
{
    [ApiController]
    [Route("api/game")]
    public class GamesController : ControllerBase
    {
        private readonly DataContext _context;
        public GamesController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.GamesList!.Include(x => x.Players)!.ThenInclude(x=>x.Results).Include(x=>x.Players)!.ThenInclude(x=>x.Points).Include(x=>x.Events));
        }

        [HttpGet("{id}")]
        public IActionResult GetDetails(int? id)
        {
            var game = _context.GamesList!.Find(id);
            if (game == null)
            {
                return NotFound();
            }

            return Ok(game);
        }
        [HttpPost]
        public IActionResult Create([FromBody] Game game)
        {
           var dbGame = _context.GamesList!.Find(game.Id);
            if (dbGame ==  null)
            {
                _context.Add(game);
                _context.SaveChanges();

                return CreatedAtAction(nameof(GetDetails), new { Id = game.Id }, game);
            }

            return Conflict();
        }
        [HttpPut("{id}")]
        public IActionResult Update(int? id, [FromBody] Game game)
        {
            if (id != game.Id || !_context.GamesList!.Any(e => e.Id == id))
            {
                return NotFound();
            }

            _context.Update(game);
            _context.SaveChanges();

            return NoContent();
        }
    }
}