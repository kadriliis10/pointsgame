using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using YLEHEKI_API.Models;

namespace YLEHEKI_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PopularGameController : ControllerBase
    {
        private readonly DataContext _context;
        public PopularGameController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.PopularGamesList!.Include(x => x.Players)!.ThenInclude(x=>x.Results).Include(x=>x.Players)!.ThenInclude(x=>x.Points).Include(x=>x.Events));
        }
        [HttpGet("{id}")]
        public IActionResult GetDetails(int? id)
        {
            var game = _context.PopularGamesList!.Include(x => x.Players)!.ThenInclude(x=>x.Results).Include(x=>x.Players)!.ThenInclude(x=>x.Points).Include(x=>x.Events);
            var tempGame = game.FirstOrDefault(x => x.Id == id);
            if (tempGame == null)
            {
                return NotFound();
            }

            return Ok(tempGame);
        }
    }
}