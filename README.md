# POINTSGAME


## Project Description

Started as a school project and then continuous development started as a hobby. Project is basically a web page where you can enter results and a points of game. You can create your own game or use a template.

## For Start

Create a SSH key pair and add it in GitLab.
Then clone the project and make sure it is connected to the repository.

## Teams/Members

- Kasper Sinisoo
- Henri Haugas
- Kadri-Liis Kolk
- Emilia Niilo
