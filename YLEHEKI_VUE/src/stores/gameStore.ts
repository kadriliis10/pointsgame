import useApi from '@/model/api';
import { Game } from '@/model/game';
import { Player } from '@/model/game';
import { Event } from '@/model/game';
import { defineStore } from 'pinia';
import { ref } from 'vue';
import router from '@/router';

export const useGamesStore = defineStore('gamesStore', () => {
  const apiGetGames = useApi<Game[]>('game');
  const games = ref<Game[]>([]);
  let liveGame = ref({
    id: 0,
    user: '',
    name: '',
    isPublic: true,
    rules: '',
    players: Array<Player>(),
    events: Array<Event>(),
    picturePath: '',
  });

  const loadGames = async () => {
    await apiGetGames.request();

    if (apiGetGames.response.value) {
      return apiGetGames.response.value!;
    }

    return [];
  };

  const load = async () => {
    games.value = await loadGames();
  };

  const getGameById = async (id: number) => {
    var allGames = await loadGames();
    const tempGame: Game = allGames.find((game: Game) => game.id === id)!;
    liveGame.value = tempGame;
    router.push('/game');
  };

  const addGame = async (game: Game) => {
    const apiAddGame = useApi<Game>('game', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(game),
    });

    await apiAddGame.request();
    if (apiAddGame.response.value) {
      games.value.push(apiAddGame.response.value!);
      await getGameById(apiAddGame.response.value!.id);
    }
  };
  const updateGame = async (game: Game) => {
    const apiUpdateGame = useApi<Game>('game/' + game.id, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(game),
    });

    await apiUpdateGame.request();
    if (apiUpdateGame.response.value) {
      load();
    }
    liveGame.value = game;
  };

  return {
    games,
    load,
    addGame,
    liveGame,
    getGameById,
    updateGame
  };
});
