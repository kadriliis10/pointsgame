import { Game } from '@/model/game';
import { Player } from '@/model/game';
import { Event } from '@/model/game';
import { defineStore } from 'pinia';
import { ref } from 'vue';

export const usePopularGamesStore = defineStore('PopularGamesStore', () => {
  const popularGames = ref<Game[]>([
    {
      id: 1,
      user: '',
      name: 'Meeste 10-võistlus',
      isPublic: true,
      rules:
        '1. Kui vähegi võimalik, peab mitmevõistluse peakohtunik tagama igale osavõtjale vähemalt 30-minutilise pausi eelmise ala lõpu ja järgmise ala alguse vahel.\n \
      2. Võistlusjärjekord võidakse loosida enne iga ala algust. 100 \
      ja 400 meetri jooksudes ning 110 meetri tõkkejooksus paigutab\
      võistlejad jooksudesse tehniline delegaat. Seejuures peab igas\
      jooksus olema 5 või enam, kuid mitte mingil juhul vähem kui 3\
      võistlejat. \n \
      3. Mitmevõistluse viimasel alal paigutatakse pärast eelviimast\
      ala liidergruppi kuuluvad võistlejad ühte jooksu. Teised\
      võistlejad võidakse paigutada (loosida) jooksudesse vastavalt\
      sellele, millal nad lõpetavad eelviimase ala. \n \
      4. Mitmevõistluse peakohtunikul on õigus vajaduse korral muuta\
      mis tahes jooksu koosseisu. \n \
      5. Pärast iga ala lõppu tuleb punktiseis (st antud ala\
      tulemusele vastavad punktid IAAF-i ametliku punktitabeli alusel\
      ja punktide summa pärast kõiki seni lõppenud alasid) kõigile\
      võistlejatele teatavaks teha.',
      players: [{ id: 0, name: '', results: [], points: [] }],
      events: [
        { id: 1, name: '100m jooks', formula: '' },
        { id: 2, name: 'Kaugushüpe', formula: '' },
        { id: 3, name: 'Kuulitõuge', formula: '' },
        { id: 4, name: 'Kõrgushüpe', formula: '' },
        { id: 5, name: '400m jooks', formula: '' },
        { id: 6, name: '110m tõkkejooks', formula: '' },
        { id: 7, name: 'Vasaraheide', formula: '' },
        { id: 8, name: 'Teivashüpe', formula: '' },
        { id: 9, name: 'Odavise', formula: '' },
        { id: 10, name: '1500m jooks', formula: '' },
      ],
      picturePath: 'src/images/KevinMayer.jpg',
    },
    {
      id: 2,
      user: '',
      name: 'Meeste 7-võistlus',
      isPublic: true,
      rules:
        'Meeste seitsmevõistlusesse kuuluvad järgmised alad, mis\
      peetakse kahel järjestikusel päeval järgnevas järjestuses:\
      Esimene päev: 60m jooks, kaugushüpe, kuulitõuge ja kõrgushüpe.\
      Teine päev: 60m tõkkejooks, teivashüpe ja 1000m jooks. \n \
      ',
      players: [{ id: 0, name: '', results: [], points: [] }],
      events: [
        { id: 1, name: '60m jooks', formula: '' },
        { id: 2, name: 'Kaugushüpe', formula: '' },
        { id: 3, name: 'Kuulitõuge', formula: '' },
        { id: 4, name: 'Kõrgushüpe', formula: '' },
        { id: 5, name: '60m tõkkejooks', formula: '' },
        { id: 6, name: 'Teivashüpe', formula: '' },
        { id: 7, name: '1000m jooks', formula: '' },
      ],
      picturePath: 'src/images/KristjanHauss.jpg',
    },
    {
      id: 3,
      user: '',
      name: 'Naiste 7-võistlus',
      isPublic: true,
      rules:
        'Naiste seitsmevõistlus koosneb seitsmest alast, mis peetakse\
      kahel, üksteisele järgneval päeval järgmises järjekorras:\
      Esimene päev - 100m tõkkejooks, kõrgushüpe, kuulitõuge ja 200m\
      jooks; Teine päev - kaugushüpe, odavise ja 800m jooks.',
      players: [{ id: 0, name: '', results: [], points: [] }],
      events: [
        { id: 1, name: '100m tõkkejooks', formula: '' },
        { id: 2, name: 'Kõrgushüpe', formula: '' },
        { id: 3, name: 'Kuulitõuge', formula: '' },
        { id: 4, name: '200m jooks', formula: '' },
        { id: 5, name: 'Kaugushüpe', formula: '' },
        { id: 6, name: 'Odavise', formula: '' },
        { id: 7, name: '800m jooks', formula: '' },
      ],
      picturePath: 'src/images/JohnsonThompson.jpg',
    },
    {
      id: 4,
      user: '',
      name: 'Naiste 5-võistlus',
      isPublic: true,
      rules:
        'Naiste viievõistlus koosneb viiest alast, mis peetakse\
        ühel päeval järgmises järjekorras:\
        60m tõkkejooks, kõrgushüpe, kuulitõuge, kaugushüpe ja 800m jooks.',
      players: [{ id: 0, name: '', results: [], points: [] }],
      events: [
        { id: 1, name: '60m tõkkejooks', formula: '' },
        { id: 2, name: 'Kõrgushüpe', formula: '' },
        { id: 3, name: 'Kuulitõuge', formula: '' },
        { id: 4, name: 'Kaugushüpe', formula: '' },
        { id: 5, name: '800m jooks', formula: '' },
      ],
      picturePath: 'src/images/PippiLottaEnok.jpg',
    },
  ]);

  const addPopularGame = (popularGame: Game) => {
    popularGames.value.push(popularGame);
  };

  return { popularGames, addPopularGame };
});
