import { createApp } from 'vue';
import App from './App.vue';
import { createPinia } from 'pinia';
import 'virtual:windi.css';
import PrimeVue from 'primevue/config';
import 'primevue/resources/themes/saga-blue/theme.css'; //theme
import 'primevue/resources/primevue.min.css'; //core css
import 'primeicons/primeicons.css'; //icons
import router from './router';
import { setApiUrl } from '@/model/api';

import DataTable from 'primevue/datatable';
import Column from 'primevue/column';

const app = createApp(App);
const getRuntimeConf = async () => {
  const runtimeConf = await fetch('/runtime-config.json');
  return await runtimeConf.json();
};

getRuntimeConf().then((json) => {
  setApiUrl(json.API_URL);

  app.use(createPinia());
  app.use(PrimeVue);
  app.use(router);

  app.mount('#app');
});
