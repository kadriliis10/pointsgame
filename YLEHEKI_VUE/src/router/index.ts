import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import newGameVue from '@/views/newGameView.vue';
import homeVue from '../views/homeView.vue';
import LogIn from '../views/logInView.vue';
import Register from '../views/registerView.vue';
import Game from '../views/gameView.vue';
import PopularGame1 from '../components/popularGamesComponents/PopularGame1.vue';
import PopularGame2 from '../components/popularGamesComponents/PopularGame2.vue';
import PopularGame3 from '../components/popularGamesComponents/PopularGame3.vue';
import PopularGame4 from '../components/popularGamesComponents/PopularGame4.vue';
import Leaderboard from '../components/leaderboard.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: homeVue,
  },
  {
    path: '/newgame',
    name: 'New Game',
    component: newGameVue,
  },
  {
    path: '/game',
    name: 'Game',
    component: Game,
  },
  {
    path: '/login',
    name: 'Log in',
    component: LogIn,
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
  },
  {
    path: '/Meeste10-v%C3%B5istlus',
    name: 'Meeste10-võistlus',
    component: PopularGame1,
  },
  {
    path: '/Meeste7-v%C3%B5istlus',
    name: 'popularGame2',
    component: PopularGame2,
  },
  {
    path: '/Naiste7-v%C3%B5istlus',
    name: 'popularGame3',
    component: PopularGame3,
  },
  {
    path: '/Naiste5-v%C3%B5istlus',
    name: 'popularGame4',
    component: PopularGame4,
  },
  {
    path: '/leaderboard',
    name: 'leaderboard',
    component: Leaderboard,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
