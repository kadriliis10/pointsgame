export interface Game {
  id: number;
  user: any;
  name: string;
  isPublic: boolean;
  rules: string;
  players: Array<Player>;
  events: Array<Event>;
  picturePath: string;
}

export class Player {
  id?: number;
  name!: string;
  results!: Array<Results>;
  points!: Array<Points>;
}

export class Points {
  point!: number;
}

export class Results {
  result!: number;
}

export class Event {
  id!: number;
  name!: string;
  formula!: string;
}

export interface Games {
  games: Game[];
}
